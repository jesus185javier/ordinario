/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ordinario;

/**
 *
 * @author jesus
 */
public class reciboNom {
    private int num;
    private String nombre;
    private int puesto;
    private int nivel;
    private int dias;
    
    public reciboNom(){
        this.num=0;
        this.nombre="";
        this.puesto=0;
        this.nivel=0;
        this.dias=10;
        
    }
    public reciboNom(int num, String nombre, int puesto,int nivel,int dias){
        this.num=num;
        this.nombre=nombre;
        this.puesto=puesto;
        this.nivel=nivel;
        this.dias=dias;
    }
    public reciboNom(reciboNom otro){
        this.num=otro.num;
        this.nombre=otro.nombre;
        this.puesto=otro.puesto;
        this.nivel=otro.nivel;
        this.dias=otro.dias;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    public float calcularPago(){
    float pago=0.0f;
    if(this.puesto ==1){
        pago=(this.dias*100);    
     }
    if(this.puesto ==2){
       pago=(this.dias*200); 
    }
    if(this.puesto ==3){
     pago=(this.dias*300);   
    }
    return pago;
    }
    public float calcularImpuesto(){
        float impuesto=0.0f;
        if(this.nivel ==1){
            impuesto=(this.calcularPago()*0.05f);
        }
   if(this.nivel ==2){
   impuesto=(this.calcularPago()*0.03f);
   }
   return impuesto;
   }
    public float calcularTotal(){
        float total=0.0f;
        total=this.calcularPago()-this.calcularImpuesto();
        return total;
    }
}

